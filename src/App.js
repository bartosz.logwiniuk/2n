import React from 'react';
import { Router } from '@reach/router';

import './styles/App.scss';
import Header from './components/Header';
import About from './views/About';
import Home from './views/Home';
import Service from './views/Service';
import Portfolio from './views/Portfolio';
import Team from './views/Team';

function App() {
  return (
    <div className="App">
      <Header />
      <Router className="router__container">
        <Home path="/" />
        <About path="/about" />
        <Service path="/services" />
        <Portfolio path="/portfolio" />
        <Team path="/team" />
      </Router>
    </div>
  );
}

export default App;
