import React from 'react';
import Navigation from './Navigation';

const Header = () => {
  return (
    <nav className="header__navigation">
      <Navigation />
    </nav>
  );
};

export default Header;
