import React from 'react';
import NavigationLink from './NavigationLink';

const Navigation = () => {
  return (
    <ul className="header__list">
      <NavigationLink path="/" label="2n" />
      <NavigationLink path="/about" label="About us" />
      <NavigationLink path="/services" label="Services" />
      <NavigationLink path="/portfolio" label="Portfolio" />
      <NavigationLink path="/team" label="Team" />
      <NavigationLink path="/blog" label="Blog" />
      <NavigationLink path="/contact" label="Contact" />
    </ul>
  );
};

export default Navigation;
