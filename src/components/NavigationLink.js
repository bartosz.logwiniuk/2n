import React from 'react';
import { Link } from '@reach/router';

const NavigationLink = props => {
  const { label, path } = props;
  return (
    <li className="header__item">
      <Link
        {...props}
        getProps={({ isCurrent }) => {
          return {
            style: {
              color: isCurrent ? 'blue' : 'white',
            },
          };
        }}
        className="header__link"
        to={path}
      >
        {label}
      </Link>
    </li>
  );
};

export default NavigationLink;
