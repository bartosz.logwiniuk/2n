import React from 'react';

const Figure = props => {
  return (
    <figure className="about-us__figure">
      <img src={props.src} alt="" />
      <figcaption className="about-us__figcaption">{props.text}</figcaption>
    </figure>
  );
};

export default Figure;
