import React from 'react';
import { Link } from '@reach/router';

const button = props => {
  const { path, style, text } = props;
  return (
    <button className="btn" style={style}>
      <Link to={path}>{text}</Link>
    </button>
  );
};

export default button;
