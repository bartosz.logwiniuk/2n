import React from 'react';

const Card = props => {
  return (
    <figure className="portfolio__card">
      <img src={props.img} alt={props.alt} className="portfolio__image" />
    </figure>
  );
};

export default Card;
