import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const Card = props => {
  return (
    <div className="service__card">
      <FontAwesomeIcon icon={props.icon} />
      <h5 className="serivce__card-titile">{props.technology}</h5>
    </div>
  );
};

export default Card;
