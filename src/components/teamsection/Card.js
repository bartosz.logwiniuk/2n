import React from 'react';

const Card = props => {
  return (
    <figure className="team__member">
      <img src={props.img} alt={props.alt} className="team__image" />
    </figure>
  );
};

export default Card;
