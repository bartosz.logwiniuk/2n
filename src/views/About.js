import React from 'react';
import grafika1 from '../images/grafika1.png';
import grafika2 from '../images/grafika2.png';
import grafika3 from '../images/grafika3.png';
import grafika4 from '../images/grafika4.png';
import Figure from '../components/aboutsection/imgs/Figure';

const About = () => {
  return (
    <article className="about-us__container">
      <section>
        <header className="about-us__header">
          <h2>About us</h2>
        </header>
        <p className="about-us__description">
          2n is a Software House, specialized in enterprise solutions. We
          believe that any complex problem could be solved easily if only we can
          sit together, and track all the reasons that make it "complex". Our
          team is passionate about what we do, loves to learn new technologies
          and is always excited to get knowledge from new business domains.
        </p>
      </section>
      <Figure src={grafika1} text="Focus" />
      <Figure src={grafika2} text="Teamwork" />
      <Figure src={grafika3} text="Expertise" />
      <Figure src={grafika4} text="Passion" />
    </article>
  );
};

export default About;
