import React from 'react';
import Button from '../components/buttons/button';

const Home = () => {
  const aboutButton = {
    color: 'white',
    backgroundColor: 'blue',
  };
  const estimateButton = {
    color: 'black',
    backgroundColor: 'white',
  };
  return (
    <main className="home__main">
      <header className="home__header">
        <h1>
          We create dedicated software for companies, bring ideas to life and
          enjoy what we do.
        </h1>
      </header>
      <Button path="/about" style={aboutButton} text="Learn more" />
      <Button path="/estimate" style={estimateButton} text="Estimate project" />
    </main>
  );
};

export default Home;
