import React from 'react';
import Card from '../components/portfoliosection/Card';
import CipherLogo from '../images/portfolio/cipher-logo.png';
import DobrygraczLogo from '../images/portfolio/dobrygracz-logo.png';
import FashLogo from '../images/portfolio/fash-logo.png';
import GalaktusLogo from '../images/portfolio/galaktus-logo.png';
import CridoLogo from '../images/portfolio/crido-logo.png';
import NauLogo from '../images/portfolio/nau-logo.png';
import ReasLogo from '../images/portfolio/reas-logo.png';
import RivelLogo from '../images/portfolio/rivel-logo.png';
import ShowfieldsLogo from '../images/portfolio/showfields-logo.png';
import TalebookLogo from '../images/portfolio/talebook.svg';

const Portfolio = () => {
  return (
    <section className="portfolio">
      <header>
        <h2 className="title title--portfolio">Our Clients</h2>
      </header>
      <div className="portfolio__cards">
        <Card img={CipherLogo} alt={'Cipher Logo'} />
        <Card img={DobrygraczLogo} alt={'Dobrygracz Logo'} />
        <Card img={FashLogo} alt={'Fash Logo'} />
        <Card img={GalaktusLogo} alt={'Galaktus Logo'} />
        <Card img={CridoLogo} alt={'Crido Logo'} />
        <Card img={NauLogo} alt={'Nau Logo'} />
        <Card img={ReasLogo} alt={'Reas Logo'} />
        <Card img={RivelLogo} alt={'Rivel Logo'} />
        <Card img={ShowfieldsLogo} alt={'Showfields Logo'} />
        <Card img={TalebookLogo} alt={'Talebook Logo'} />
      </div>
    </section>
  );
};

export default Portfolio;
