import React from 'react';
import Card from '../components/servicesection/Card';
import {
  faGem,
  faLaptopCode,
  faUserFriends,
  faChartBar,
  faRocket,
  faBug,
  faBuilding,
  faDatabase,
} from '@fortawesome/free-solid-svg-icons';

const Service = () => {
  return (
    <section className="service">
      <div className="service__description">
        <header>
          <h2 className="title-service">Services</h2>
        </header>
        <p className="description-service">
          We are specializing in advanced software development. With over 20
          experienced developers and 10+ years in digital technology we can
          deliver most advanced solutions.
        </p>
      </div>
      <div className="service__cards-container">
        <div className="service__cards-row">
          <Card icon={faGem} technology={'Ruby Development'} />
          <Card icon={faLaptopCode} technology={'Frontend Development'} />
          <Card icon={faUserFriends} technology={'Devops Services'} />
          <Card icon={faChartBar} technology={'Expertise Products'} />
        </div>
        <div className="service__cards-row">
          <Card icon={faRocket} technology={'MVP Products'} />
          <Card icon={faBug} technology={'Refactoring Legacy Code'} />
          <Card icon={faBuilding} technology={'Business Processes'} />
          <Card icon={faDatabase} technology={'Working with complex API'} />
        </div>
      </div>
    </section>
  );
};

export default Service;
