import React from 'react';
import Card from '../components/teamsection/Card';
import Adam from '../images/team/adam.jpg';
import Blank from '../images/team/blank.png';
import Ardi from '../images/team/ardi.jpg';
import Darek from '../images/team/darek.jpg';
import Dyzio from '../images/team/dyzio.jpg';
import Furia from '../images/team/furia.jpg';
import Kamil from '../images/team/kamil.jpg';
import Lukasz from '../images/team/lukasz.jpg';
import Mehdi from '../images/team/mehdi.jpg';
import Pawel from '../images/team/pawel.jpg';
import Rafal from '../images/team/rafal.jpg';
import Bartek from '../images/team/bartek.jpg';

const Team = () => {
  return (
    <section className="team">
      <header>
        <h2 className="title title--team">Meet The Team</h2>
      </header>
      <div className="team__members">
        <Card img={Adam} alt={'Adam'} description={'CTO'} />
        <Card img={Blank} alt={'Martiusz'} description={'CEO'} />
        <Card img={Ardi} alt={'Adrian'} description={'Ruby Developer'} />
        <Card img={Darek} alt={'Darek'} description={'Ruby Developer'} />
        <Card img={Kamil} alt={'Kamil'} description={'Ruby Developer'} />
        <Card img={Lukasz} alt={'Łukasz'} description={'Project Manager'} />
        <Card img={Mehdi} alt={'Mehdi'} description={'Ruby Developer'} />
        <Card img={Pawel} alt={'Paweł'} description={'Ruby Developer'} />
        <Card img={Rafal} alt={'Rafał'} description={'Fullstack Developer'} />
        <Card img={Bartek} alt={'Bartek'} description={'Frontend Developer'} />
        <Card img={Blank} alt={'Krzysiek'} description={'Developer'} />
        <Card img={Blank} alt={'Kuba'} description={'Developer'} />
        <Card img={Blank} alt={'Marek'} description={'Developer'} />
        <Card img={Blank} alt={'Łukasz'} description={'Frontend Developer'} />
        <Card img={Blank} alt={'Paweł'} description={'Frontend Developer'} />
        <Card img={Dyzio} alt={'Dyzio'} description={'Pet'} />
        <Card img={Furia} alt={'Furia'} description={'Pet'} />
      </div>
    </section>
  );
};

export default Team;
